This module sets up the email configuration that I've used on several projects. It's just a simple module that has some dependencies and does some really basic configuration (setting up a filter for email use).

Dependencies: 

* commerce_message (for commerce message types and the message stack)
* emogrifier (to handle style inlining)
* mailsystem (for email)
* htmlmail (for doing mime)

During installation of the module, some theme variables are set and the filter is updated to include the emogrifier. Inline comments should walk you through it.